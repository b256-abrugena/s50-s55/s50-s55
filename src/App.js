// This import code is already included during the intallation of the app.
import './App.css';

// S50: Imported from AppNavBar.js. At first, import code for Banner was also added in App.js, however it was omitted since it links from pages such as Home and ErrorPage. Import code for Highlights was also added in App.js, however it was also omitted afterwards.
import AppNavBar from './components/AppNavBar'; // From S50 Discussion
import { Container } from 'react-bootstrap'; // From S50 Discussion

import Home from './pages/Home'; // From S51 discussion
import Courses from './pages/Courses'; // From S51 discussion 

import Register from './pages/Register'; // From S52 discussion
import Login from './pages/Login'; // From S52 activity

/*S53: The `BrowserRouter` component will enable us to simulate page navigation by synchronizing the shown content and the shown URL in the web browser. This is to ensure that the content na sinabi natin kay computer na ilagay on the certain route, yun din ang ipapakita sa browser.

The `Routes` component holds all our Route components. It selects which `Route` component to show based on the URL Endpoint.*/
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'; // From S53 Discussion
import Logout from './pages/Logout'; // From S53 Discussion
import ErrorPage from './pages/ErrorPage'; // From S53 Activity

import { useState, useEffect } from 'react';
import { UserProvider } from './userContext';
import CourseView from './pages/CourseView';

function App() {

  // State hook for the user to store the information for a global scope
  // Initialized as an object with properties from the localStorage
  // This will be used to store the user information and will be used for validating if a user is logged in on the app or not
  const [user, setUser] = useState({
      id: null,
      isAdmin: null
  });

  // Function for clearing the localStorage when a user logout
  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    console.log(user)
    console.log(localStorage)
  })

  return (
    // S50: Fragments <> and </>
    // S50: Fragments are needed when there are two or more components, pages or html elements present in the code.
    <UserProvider value={{user, setUser, unsetUser}}>

      {/*S53: Addition of Router in App*/}
      <Router>
        {/* Self Closing Tags */}
          <AppNavBar />

          {/*Container from S50 discussion*/}
          <Container>

              {/*S53: Addition of Routes with specific path and element for each page in the App.
              Lagi dapat nakapaloob si Routes kay Router*/}
              <Routes>

                {/*S53: Path is the endpoint of the application, and element is the page.

                Routes are used para baguhin ang URL ng page
                Link/NavLink are used para baguhin ang NavBar
                */}
                <Route path="/" element={<Home />} />
                <Route path="/courses" element={<Courses />} />
                <Route path="/register" element={<Register />} />
                <Route path="/login" element={<Login />} />
                <Route path="/logout" element={<Logout />} />
                <Route path="/courseView/:courseId" element={<CourseView />} />
                <Route path="*" element={<ErrorPage />}/>
              </Routes>

          </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
