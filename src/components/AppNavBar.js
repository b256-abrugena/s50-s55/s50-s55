// S50: React Bootstrap Components added to AppNavbar
// Deconstructed components for React Bootstrap Navbar
import { Container, Navbar, Nav } from 'react-bootstrap';

import { Link, NavLink } from 'react-router-dom'; // Added last S53 discussion

import { useContext } from 'react';
import UserContext from '../userContext';

/*
	Notes from S50:
	1. Components serve as reusable pieces of code, such as the Navbar.
	2. As best practice, the name of the function should be the same as the name of the component that you are creating.
	3. In this case, since the name of the component is AppNavBar.js, the function should be AppNavBar as well. 
	4. Inside of a function, there should be a "return", which will contain the contents of the component inside the webpage.
	5. Each component consists of Bootstrap, HTML and JS codes inside of its respective fucntion. (All in one file)
*/
export default function AppNavBar(){
	
	/*
		S53 Code:

		import{ useState } from 'react';

		// State hooks to store user information in the login page
		// We get the "email" as variable from Login page from localStorage.getItem to check if there are already logged in in the Login page.
		const [user, setUser] = useState(localStorage.getItem("email"));
		console.log(user);

	*/

	
	const { user } = useContext(UserContext);

	// S50: The content inside the return will display the contents for the AppNavBar component. 
	return(
		// S50: Navbar is one of the primary elements present in a web application/page
		// S50: Link for Bootstrap Navbar: https://react-bootstrap.github.io/components/navbar
		<Navbar bg="light" expand="lg">
	      <Container>

	      	{/*S53: Navbar.Brand will work as Link going to home page as "/" */}
	        <Navbar.Brand as={Link} to="/">Zuitt Bootcamp</Navbar.Brand>
	        <Navbar.Toggle aria-controls="basic-navbar-nav" />
	        <Navbar.Collapse id="basic-navbar-nav">
	          <Nav className="me-auto">
	            <Nav.Link as={NavLink} to="/">Home</Nav.Link>
	            <Nav.Link as={NavLink} to="/courses">Courses</Nav.Link>

	            {/*S53: Addition of ternary operator in the case whether the user logs in the page or not*/}
	            {
	            	(user.id !== null) ?
	            		<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
	            	:
	            	// Fragments <> and </>
    				// Fragments are needed when there are two or more components, pages or html elements present in the code.
    				// Login and Register were created last S53 Discussion 
	            	<>
	            		<Nav.Link as={NavLink} to="/login">Login</Nav.Link> 
	            		<Nav.Link as={NavLink} to="/register">Register</Nav.Link>
	            	</>
	            }
	          </Nav>
	        </Navbar.Collapse>
	      </Container>
	    </Navbar>
	)

}