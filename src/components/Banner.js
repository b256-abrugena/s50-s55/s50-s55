/*
	S50 NOTES: 

	*Old import codes from S50:
		import Button form 'react-bootstrap/Button';
		import Row from 'react-bootstrap/Row';
		import Col from 'react-bootstrap/Col';

	*Destructuring React Bootstrap Components using "{}". 
	By adding these curly braces, you will delete the specific endpoint to obtain a cleaner code.
	ex: From
		import Row from 'react-bootstrap/Row';

		To 
		import { Row } from 'react-bootstrap/';

	And since in this case that the three items have the same source which is 'react-bootstrap', you can combine this into one {}.
	ex: From
		import { Button } form 'react-boogtstrap';
		import { Row } from 'react-bootstrap';
		import { Col }  from 'react-bootstrap';

		To
		import { Button, Row, Col } from 'react-bootstrap';
*/
import { Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';


// S50: As best practice, the name of the function should be the same as the name of the component that you are creating.
// S50: Each component consists of Bootstrap, HTML and JS codes inside of its respective fucntion. (All in one file)
/*
	From S50:
	Old code for Banner component

	export default function Banner(){
	
		return(
			// We use these codes because of the JSX, in which instead of creating a separate HTML file, you can encode this in just one file only.
			<Row>
				<Col className="p-5 text-center">
					<h1>Zuitt Coding Bootcamp</h1>
					<p>Opportunities for everyone, everywhere!</p>
					<Button variant="primary">Enroll Now!</Button>
				</Col>
			</Row>
		)

	}
*/
export default function Banner({data}) {

    const {title, content, destination, label} = data;
    console.log(data);

	return(
		<Row>
			<Col className="p-5 text-center">
				<h1>{title}</h1>
				<p>{content}</p>
				<Button as={Link} to={destination} variant="primary">{label}</Button>
			</Col>
		</Row>
	)
}
