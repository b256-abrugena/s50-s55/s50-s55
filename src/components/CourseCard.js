// Created in S50 

// import { useState } from 'react'; --> This is for the hooks (Ccded during S51 discussion)
// useState is used if we're going to check the state of the code.

import { Link } from 'react-router-dom';
import { Button, Card } from 'react-bootstrap';

// properties (or props) acts as a function parameter
/*
	props = courseProp : {
		 description: "Nostrud velit dolor excepteur ullamco consectetur aliquip tempor. Consectetur occaecat laborum exercitation sint reprehenderit irure nulla mollit. Do dolore sint deserunt quis ut sunt ad nulla est consectetur culpa. Est esse dolore nisi consequat nostrud id nostrud sint sint deserunt dolore.",
		 id: "wdc001",
		 name: "PHP - Laravel",
		 onOffer: true,
		 price: 45000
	}     
*/


export default function CourseCard ({courseProp}){
	
	/*
		S50 Activity Solution:
		return(
			<Card>
		      <Card.Body>
		        <Card.Title>Sample Course</Card.Title>
		        <Card.Subtitle>Description:</Card.Subtitle>
		        <Card.Text>This is a sample course offering.</Card.Text>
		        <Card.Subtitle>Price:</Card.Subtitle>
		        <Card.Text>PhP 40,000</Card.Text>
		        <Button variant="primary">Enroll</Button>
		      </Card.Body>
		    </Card>
		)
	*/

	/*
		Encoded from S51 discussion
		At first, the argument that was set to this function is "props".

		export default function CourseCard(props){
			console.log(props);
			console.log(typeof props);
			
				return(
					<Card>
				      <Card.Body>
				        <Card.Title>{props.courseProp.name}</Card.Title>
				        <Card.Subtitle>{props.courseProp.deescription}</Card.Subtitle>
				        <Card.Text>This is a sample course offering.</Card.Text>
				        <Card.Subtitle>Price:</Card.Subtitle>
				        <Card.Text>{props.courseProp.price}</Card.Text>
				        <Button variant="primary">Enroll</Button>
				      </Card.Body>
				    </Card>
				)
		}
	*/
	
	// S51: To set a "specific value" as the parameter of the function, it should be enclosed by curly braces "{}".
	// S51: If you want to set the specific value as the parameter/argument of this function, it was changed from "props" to "{courseProp}", since it was named in the Course.js under the <CourseCard />

	// S51: Object Destructuring
	// S51: This is to remove courseProp in assigning name, description, price, and _id
	// This is based on the object "courseProp" in which data was shared.
	/*
	courseProp = {
		 description: "Nostrud velit dolor excepteur ullamco consectetur aliquip tempor. Consectetur occaecat laborum exercitation sint reprehenderit irure nulla mollit. Do dolore sint deserunt quis ut sunt ad nulla est consectetur culpa. Est esse dolore nisi consequat nostrud id nostrud sint sint deserunt dolore.",
		 id: "wdc001",
		 name: "PHP - Laravel",
		 onOffer: true,
		 price: 45000
	}
	*/
	const { name, description, price, _id } = courseProp;

	/*
		S51 Notes for useState
		useState is used if we are going to check the state of the code.

		// getter saves/stores the value that acts as a variable as well.
		// setter tells the computer how to store the value, it sets the value to be stored in the getter
		// getter and setter works as a pair in useState
		// "0" as the initial Getter Value in the case of count and setCount
		// at the start of the function, the value of count is set to zero
		
		const [count, setCount] = useState(0);
		const [seats, setSeats] = useState(30)

		function enroll (){
	 		if(seats > 0){

	 			// setCount is a method that tells you what to do
	 			// it will only trigger during the "enroll" function 
				setCount(count + 1);
				console.log('Enrollees: ' + count);

				setSeats(seats - 1);
				console.log('Seats: ' + seats);
	 		}
	 		else{
				alert("No more seats available");
	 		};
		}
	*/


	

	// 	// My answer for S51 Activity
	// 	if(count === 30){
	// 		setCount(count);
	// 		alert("No more seats.")
	// 	}
	// }

	return(
		<Card className="courseCard p-3">
	      <Card.Body>
	        <Card.Title>{name}</Card.Title>
	        <Card.Subtitle>Description:</Card.Subtitle>
	        <Card.Text>{description}</Card.Text>
	        <Card.Subtitle>Price:</Card.Subtitle>
	        <Card.Text>{price}</Card.Text>
	        <Button variant="primary" as={Link} to={`/courseView/${_id}`}>Details</Button>
	      </Card.Body>
	    </Card>
	)

}