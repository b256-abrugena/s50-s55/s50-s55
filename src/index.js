/*
  S50 Inroduction to JS Notes

  - By using React.js, we can create components or pages with reusable parts in order to minimize the number of files/codes that we have in creating a page/application in order to become more efficient to use.

  - React.js is a JavaScript library for building user interfaces that is:
      Declarative: It makes you write code with the question of WHAT, instead of HOW something is to be done. It also allows your code to be more predictable and easier to debug. 

      Component-based: Allows you to write reusable, complex UI components in a quick and efficient manner. 
      
      Learn Once, Write Anywhere: React can also be used on the server using Node and power mobile apps using React Native. 

  - React.js is a framework, having a foundation na pwedeng gamitin or ireuse, just like Express.js having libraries and commands na pwedeng gamitin.

  - React is used for web development or web applications
  - React Native is used for mobile applications

  - React.js solves the problems in applications that require frequent and rapid changes in page data that are resource-intensive and complicated to code. 

  - React.js separate concerns into components rather than technologies instead of the conventional HTML for structure, CSS for styling, and JavaScript for interactivity. 

  - React.js applies rapid updates using virtual DOM. This only works on the differences between the current DOM state and the target DOM state.

  - Virtual DOM saves the current state of your app. Only the updated element/object will refresh and not the entire page/app.

  - React.js stores information in a component using states.  
*/


// S50: Import React, ReactDOM and import App are already included in the installed package for React.js app
import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

// S50: React Bootstrap package allows us to gain access to readily made React.js components similar to bootstrap components.
// S50: Import the bootstrap CSS
import 'bootstrap/dist/css/bootstrap.min.css';


// S50: React has its own Document Object Model (DOM)
// S50: document.getElementbyId enables to update/store all changes made on a specific id from html file.
// S50: index.html that is located on the public folder contains a div section with an id name as "root"
// S50: Kung anuman ang nilalagay natin kay index.js, naka-connect siya kay index.html na nakapasok siya sa loob ng division named as 'root'.
const root = ReactDOM.createRoot(document.getElementById('root'));


// S50: Render displays the react elements into the root. 
// S50: App component is our mother component, this is the component we use as entry point and where we can render all other components or pages.
// From google: React.StrictMode is a tool that highlights potential issues in a program. it works by encapsulating a portion of your full application as a component.
// S50: All components should not be placed inside the render of index.js file. Instead, it must be included in the App.js file since this is not the most efficient way to create our application. It is recommended and a best practice that only one component is placed inside the render code, which is the "App" component. 
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);



/*
    "S50 codes" 

    // S50: The syntax for creating elements in JS is very similar to creating elements in HTML 
    // S50: JavaScript XML (JSX) allows us to create HTML elements and at the same time allows us to apply JavaScript code to these elements making it easy to write both HTML and JavaScript code in a single file as opposed to creating two separate files (One for HTML and another for JavaScript syntax)
    // S50: Instead of doing separate files for .html, .css and .js, we are doing these codes into one file only because of JSX. 

    const name = "John Smith";
    const user = {
      firstName : "Jane",
      lastName : "Smith",
    }

    function formatName(user){
      return user.firstName + ' ' + user.lastName; 
    }

    // "<h1>Hello, {name}</h1>" will reflect as "Hello, John Smith" on the app since you assign "name" variable as John Smith
    // const element = <h1>Hello, John Smith</h1>
    const element = <h1>Hello, {name} and {formatName(user)}</h1>

    // S50: render() displays the react elements/components into the root.
    // root.render(<h1>Hello, John Smith</h1>)
    root.render(element);
*/