/*
	S51 Notes

	Props in JavaScript are "Properties" in which these are the information that a component receives, usually from a parent component. It is similato to objects that are also used to transferring data.

	Props are synonymous to the function parameters. The difference is the contents/information from a parent component going to another component came to the props. 

	What is it for?

	Props are what allows components to be reusable.
	By using props, one can use the same component and feed different data to the component for rendering. 

	States in React.js allow components to create and manage its own data and is meant to be used intenrally. 
	For states, there is a function called "useState()"

	Hooks in React.js are functions that allow developers to create and manage states and lifecycle within components. 
*/

// Created during S51 discussion
// Displays the courses inside the app.

/*
	Old Code from S51:

	import courseData from '../data/CourseData';
	import CourseCard from '../components/CourseCard';

	export default function Courses(){
	
		console.log(CourseData);

		return(
			<>
				<h1>Courses</h1>

				// courseProp serves as the prop
				// "{CourseData[0]}" as a value indicated in curly braces
				// Tinatransfer natin yung information ni courseData mula kay Courses.js na file papunta kay CourseCard para mareceive ni CourseCard yung CourseData natin na "0".
				<CourseCard courseProp={CourseData[0]} />
			</>
		)

	}

*/

import { useState, useEffect } from 'react';
// import CourseData from '../data/CourseData';
import CourseCard from '../components/CourseCard';

export default function Courses(){

/*
	From S51 Code:

	console.log(CourseData);

	// By using the .map method, inisa-isa natin yung laman ng array from our database
	// Bawat isa ay nasasave sa "course" na variable which will send naman sa courseProp, followed by yung role ng courseProp sa CourseCard.js
	// "key" as unique identifier of the course 
	const courses = CourseData.map(course => {
		return(
			<CourseCard key={course.id} courseProp={course} />
		)
	})

	return(
		<>
			<h1>Courses</h1>

			// From the variable "courses" as the result of mapping data from the imported CourseData
			{courses}
		</>
	)
*/


	const[ courses, setCourses ] = useState([])

	useEffect(() => {

		fetch('http://localhost:4000/courses/active')
		.then(res => res.json())
		.then(data => {

			// console.log(data);

			setCourses(data.map(course => {
				return(
					<CourseCard key = {course.id} courseProp={course} />
				)
			}))
		})
	})

	return (
		<>
			<h1>Courses</h1>
			{courses}
		</>
	)

}