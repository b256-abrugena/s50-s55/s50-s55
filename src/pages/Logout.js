// Created during S53 discussion

import { useContext, useEffect } from 'react';
import { Navigate } from 'react-router-dom'; // Encoded from S53 Discussion
import UserContext from '../userContext';

export default function Logout(){

	/*
		S53 Code:

		localStorage.clear();

		return(
			<Navigate to ="/login" />
		)
	*/

	const { unsetUser, setUser } = useContext(UserContext);

	unsetUser();

	useEffect (() => {

		setUser({
			id: null
		})
		
	})

	return(

		<Navigate to='/login' />

	)

}