/*
	S52 Notes:

	*Effect Hooks in React allow us to execute a piece of code whenever a component gets rendered to the page or if the value of a state changes.

	*Ginagamit natin si effect hooks since may effect na nangyayari everytime na may nagrerender na component or may nagbabago ng value orstate.

	*Example: In a certain log-in form, once nacomplete mo ang pagaccomplish ng 
	email address and password, magaactivate yung log-in button sa form. 

	*This is especially useful if you want your page to be reactive in a way na may nangyayari na changes sa page. (Example: Fetching a data from the server, changing the contents of a page)

	*Handling events with React elements is very similar to handling events on DOM elements. There are some syntax differences though.
	1. React Events are named using camelCase, rather than lowercase.
	2. With JSX, you pass a function as the event handler, rather than a string.

	*For example, in HTML:
	<button onClick="activateLasers()">Activate Lasers</button>
*/

// Created during S52 discussion

import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';

import { Navigate, Link, useParams, useNavigate } from 'react-router-dom';
import UserContext from '../userContext';
import Swal from 'sweetalert2';

export default function Register(){

	const{ user, setUser } = useContext(UserContext);

	// S55 Activity Code
	const navigate = useNavigate();

	// S55 Activity Code:
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	
	// S52: State hooks to store the values of our input fields
	// S52: const["getter", "setter"] = useState("Initial Value");
	// S52: Empty String as Initial Value to the E-mail
	// S52: Empty String as Initial Value to password1 and password2
	const [email, setEmail] = useState('');

	// S55 Activity Code:
	const [mobileNo, setMobileNo] = useState('');

	// S52	
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	
	// S52: State to determine whether the submit button is enabled or not
	// S52: Submit button should only be enabled once may laman na si e-mail, password, and verify password.
	// S52: isActive determines if the button should be active
	// S52: "false" as initial state of the button, meaning hindi muna siya gagana sa umpisa
	const [isActive, setIsActive] = useState(false);

	console.log(firstName);
	console.log(lastName);
	console.log(email);
	console.log(mobileNo);
	console.log(password1);
	console.log(password2);

	// S52: useEffect() is a method that is used whenever there is a change in our webpage
	// S52: useEffect case for the Submit button once conditions are met
	// S52: setIsActive(true) will activate if email, password1 and password2 is not eempty and also if the content for password1 is the same in password2. otherwise, it will set as false. 
	useEffect (() => {
		if((firstName !== '' && lastName !== '' && email !== '' && mobileNo !== '' && password1 !=='' && password2 !=='') && (password1 === password2)){
			setIsActive(true)
		}
		else{
			setIsActive(false)
		}
	}) 

	// S52: This function will be used/activated once a form is submitted. 
	function registerUser(e){

		// S52: Prevents refreshing the page everytime we click the "Submit" button from our form. 
		e.preventDefault();

		// S55 Activity Code
		// Process a fetch request to the corresponding backend API
		fetch('http://localhost:4000/users/register', {
			method: "POST",
			headers:{
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				password: password1
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data)

			// If no user information is found, the "access" property will not be available and will return undefined
		    // Using the typeof operator will return a string of the data type of the variable/expression it preceeds which is why the value being compared is in a string data type
			if(typeof data !== "undefined"){
				localStorage.setItem('data', data)

				Swal.fire({
					title: "Registration success!",
					icon: "success",
					text: "Welcome to Zuitt!"
				})

				navigate("/login");

			}
			else{
				Swal.fire({
					title: "Registration failed",
					icon: "error",
					text: "Check your login details and try again."
				})
			}

		})

		// S52: Clear input fields
		setFirstName('');
		setLastName('');
		setEmail('');
		setMobileNo('');
		setPassword1('');
		setPassword2('');

		// S52: Creation of alert for every successful register
		// alert('Thank you for registering!'); 
	}


	return(
		// (user.email !== null) ?
		// 	<Navigate to="/courses" />
		
		// :

		// S52: Everytime na magsusubmit tayo, papaganahin natin si registerUser(). 
		// S52: onSubmit is a method used in our form in which once we click the button, since naka type="submit" siya, everytime na magsusubmit tayo ng form, mapapagana natin si "registerUser" na function. 
		<Form onSubmit={e => registerUser(e)}>

		  <Form.Group className="mb-3" controlId="formBasicFirstName">
	        <Form.Label>First Name:</Form.Label>
	        <Form.Control type="text" placeholder="Enter your first name" value={firstName} onChange={e => setFirstName(e.target.value)}/>
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="formBasicLastName">
	        <Form.Label>Last Name:</Form.Label>
	        <Form.Control type="text" placeholder="Enter your last name" value={lastName} onChange={e => setLastName(e.target.value)}/>
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="formBasicEmail">
	        <Form.Label>Email address:</Form.Label>

	        {/* 
				Notes from S52 Discussion:

	        	onChange method works that whenever we trigger or create an event in our JS, we retrieve that access to our methods. it also checks if there are any changes inside of the input fields.

	        	Nagrarun siya ng function wherein iseset niya yung email natin na variable kung anuman ang value non, isasave or ilalagay siya sa "email" as the getter.

	        	value={email} the value stored in the input field will come from the value inside the getter "email".

	        	setEmail(e.target.value) - sets the value of the getter email to value={email} inside the input field.
	        */}
	        <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)}/>
	        <Form.Text className="text-muted">
	          We'll never share your email with anyone else.
	        </Form.Text>
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="formBasicMobileNo">
	        <Form.Label>Mobile Number:</Form.Label>
	        <Form.Control type="text" placeholder="Enter your mobile number" value={mobileNo} onChange={e => setMobileNo(e.target.value)}/>
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="formBasicPassword">
	        <Form.Label>Password:</Form.Label>
	        <Form.Control type="password" placeholder="Password" value={password1} onChange={e => setPassword1(e.target.value)}/>
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="formBasicPassword2">
	        <Form.Label>Verify Password</Form.Label>
	        <Form.Control type="password" placeholder="Verify Password" value={password2} onChange={e => setPassword2(e.target.value)}/>
	      </Form.Group>

	    {/* From S52 */}
	    {/* Ternary Operator */}
	    {/*
			if(isActive == true){
				<Button variant="primary" type="submit" id="submitBtn">Submit</Button>
			}

			else{
				<Button variant="danger" type="submit" id="submitBtn" disabled>Submit</Button>
			}
	    */}
	      {
	      	isActive ? 
		      	<Button variant="primary" type="submit" id="submitBtn">Submit</Button>
		    :
		    	<Button variant="danger" type="submit" id="submitBtn" disabled>Submit</Button>
	      }
	      
	    </Form>
	)
}