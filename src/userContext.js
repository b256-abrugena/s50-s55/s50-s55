import React from 'react';

// Create a Context Project
// A context object is a data type of an object that can be used to store information that can be shared to other components within the application.
// The context object is a different approach to passing information between components and allows easier access by avoiding prop-drilling.
// prop-drilling means transferring data or information from one place to another using props
const UserContext = React.createContext();

// The "Provider" component allows other components to consume/use the context objects and supply the necessary information needed to the context object. 
export const UserProvider = UserContext.Provider;

export default UserContext;